import stampit from 'stampit'
import uuid from 'uuid/v4'
import {QType} from "../meta/QType";
import {QARRAY, QSTRING} from "../meta/QDataType";
import {QCause} from "../questions/why/QCause";
import {QParameter} from "../meta/QParameter";
import {ENUM_ID_PREFIX, QEnum} from "../meta/QEnum";

export const TYPE_COMMAND = QType({
	id: 'com.sevenq.type.command',
	title: 'Command'
})

export const QCommand = stampit(
	QCause,
	{
	name: TYPE_COMMAND.title,
	deepStatics: {
		MainLabel: TYPE_COMMAND.title,
		Type: TYPE_COMMAND,
		Attributes: {
			Id: {type: QSTRING},
			Title: {type: QSTRING},
			EmitsEvents : { type: QARRAY },
      ParametersIn : { type: QARRAY },
      ParameterOut : { type: QParameter },
		},
	},
	init({id, title, emitsEvents, parametersIn, parameterOut},{ stamp }) {
		this.meta = stamp
		this.id = id || uuid()
		this.title = title || stamp.MainLabel
		this.emitsEvents = emitsEvents || []
    this.parametersIn = parametersIn || []
    this.parameterOut = parameterOut || null
	},
	methods: {
		execute(state){
		}
	}
})

export const ENUM_COMMAND_KEYWORDS = QEnum(
  ENUM_ID_PREFIX + '.flow.command.keywords',
  "Keywords",
  ["CREATE","GET","UPDATE","DELETE"])


export const ENUM_COMMAND_KEYWORDS = QEnum(
  ENUM_ID_PREFIX + '.flow.command.keywords',
  "Keywords",
  ["CREATE","GET","UPDATE","DELETE"])


export const ENUM_COMMAND_KEYWORDS = QEnum(
  ENUM_ID_PREFIX + '.flow.command.keywords',
  "Keywords",
  ["CREATE","GET","UPDATE","DELETE"])

