import stampit from 'stampit'
import uuid from 'uuid/v4'
import {QType} from "../meta/QType";
import {QOBJECT, QSTRING} from "../meta/QDataType";
import {QEffect} from "../questions/why/QEffect";

export const TYPE_EVENT = QType({
	id: 'com.sevenq.type.event',
	title: 'Event'
})

export const QEvent = stampit(
	QEffect,
	{
	name: TYPE_EVENT.title,
	deepStatics: {
		MainLabel: TYPE_EVENT.title,
		Type: TYPE_EVENT,
		Attributes: {
			Id: {type: QSTRING},
			Title: {type: QSTRING},
			Target: {type: QOBJECT},
			Payload: {type: QOBJECT}
		},
	},
	init({id, title, target, payload},{ stamp }) {
		this.meta = stamp
		this.id = id || uuid()
		this.title = title || stamp.MainLabel
		this.target = target
		this.payload = payload
	},
	methods: {
		apply(state){
			return state
		}
	}
})