import uuid from 'uuid/v4'
import stampit from 'stampit'

import * as dataTypes from '../meta/QDataType'
import {QAttribute,TYPE_ATTRIBUTE} from "../meta/QAttribute";
import {QSTRING} from "../meta/QDataType";

export const HasTitle = stampit({
	name: "HasTitle",
	deepStatics: {
		Attributes: {
			Title : QAttribute({
				id: [TYPE_ATTRIBUTE.id, "Title"].join('.'),
				title: "Title",
				dataType: QSTRING,
				required: true,
				fieldName: "title"})
		},
	},
	init( {title}, {stamp} ){
		this[stamp.Attributes.Title.fieldName] = title
			|| (stamp.MainLabel + '[' + this[stamp.Attributes.Id.fieldName].slice(0,6) + ']')
	}
})
