import uuid from 'uuid/v4'
import stampit from 'stampit'

import * as dataTypes from '../meta/QDataType'
import {QAttribute,TYPE_ATTRIBUTE} from "../meta/QAttribute";
import {QSTRING} from "../meta/QDataType";

let TITLE_ID = "Id";
export const HasId = stampit({
	name: "HasId",
	deepStatics: {
		Attributes: {
			Id : QAttribute({
				id: [TYPE_ATTRIBUTE.id,TITLE_ID].join('.'),
				title: TITLE_ID,
				dataType: QSTRING,
				required: true,
				fieldName: TITLE_ID.toLowerCase()})
		},
	},
	init( {id}, {stamp}){
		this[stamp.Attributes.Id.fieldName] = id || uuid()
	}
})
