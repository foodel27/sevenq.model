import stampit from 'stampit'
import {HasId} from "./HasId";
import {HasTitle} from "./HasTitle";
import {QAttribute, TYPE_ATTRIBUTE} from "../meta/QAttribute";
import {QARRAY, QSTRING} from "../meta/QDataType";
import {QWord} from "./QWord";
import {QType} from "../meta/QType";

export const TYPE_RELATION = QType({
	id: 'com.sevenq.type.relation',
	title: 'Relation'
})

export const QRelation = stampit(
	HasId,
	HasTitle,
	{
		name: TYPE_RELATION.title,
		deepStatics:{
			Type: TYPE_RELATION,
			MainLabel: TYPE_RELATION.title,
			TargetTypes: [],
			SourceTypes: [],
			FieldName : "",
			Attributes: {
				Source : QAttribute({
					id: TYPE_RELATION.id + ".attribute.source",
					title: "Source",
					dataType: QWord,
					required: true,
					fieldName: "source"}),
				Target : QAttribute({
					id: TYPE_RELATION.id + ".attribute.target",
					title: "Target",
					dataType: QWord,
					required: true,
					fieldName: "target"}),
			},
			Parents: [
				TYPE_RELATION
			],
		},
		init({source, target}, {stamp} ){
			this.meta = stamp;
			this[stamp.Attributes.Title.fieldName] = this.meta.MainLabel
			this[stamp.Attributes.Source.fieldName] = source
			this[stamp.Attributes.Target.fieldName] = target
		},

	})
