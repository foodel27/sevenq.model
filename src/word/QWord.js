import uuid from 'uuid/v4'
import stampit from 'stampit'

import * as dataTypes from '../meta/QDataType'
import {QAttribute,TYPE_ATTRIBUTE} from "../meta/QAttribute";
import {QSTRING} from "../meta/QDataType";
import {HasId} from "./HasId";
import {HasTitle} from "./HasTitle";
import {QLabel,TYPE_LABEL} from "../meta/QLabel";
import {QType} from "../meta/QType";

export const TYPE_WORD = QType({
	id: 'com.sevenq.type.word',
	title: 'Word'
})


export const QWord = stampit(
	HasId,
	HasTitle,
	{
		name: TYPE_WORD.title,
		deepStatics: {
			Type: TYPE_WORD,
			MainLabel: TYPE_WORD.title,
			Labels: [
				QLabel({
					id: [TYPE_LABEL.id,TYPE_WORD.id].join('.'),
					title: TYPE_WORD.title
				})
			],
			Parents: [
				TYPE_WORD
			],
			Methods: {},
			Attributes: {},
			Relations: {},
		},
		init( {}, {stamp}){
			this.meta = stamp
		}
	})
