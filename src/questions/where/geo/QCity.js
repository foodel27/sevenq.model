import stampit from 'stampit'
import {HasPositionXY} from "../HasPositionXY";
import {QAttribute, TYPE_ATTRIBUTE} from "../../../meta/QAttribute";
import {QNUMBER} from "../../../meta/QDataType";
import {QType} from "../../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../../meta/QLabel";

export const TYPE_CITY = QType({
  id: 'com.sevenq.type.where.geo.city',
  title: 'City'
})


export const QCity = stampit(
  HasPositionXY,
  {
    name: TYPE_CITY.title,
    deepStatics: {
      Type: TYPE_CITY,
      MainLabel: TYPE_CITY.title,
      Labels: [
        QLabel({
          id: [TYPE_LABEL.id,TYPE_CITY.id].join('.'),
          title: TYPE_CITY.title
        })
      ],
      Parents: [
        TYPE_CITY
      ],
      Attributes: {
        // Population : QAttribute({
        //   id: TYPE_ATTRIBUTE.id + "." + TYPE_CITY.title + ".population",
        //   title: "Population",
        //   dataType: QNUMBER,
        //   required: false,
        //   fieldName: "population"})
      },
    },
    init( {}, {stamp} ){
    }

  }
)
