import stampit from 'stampit'
import {QAttribute, TYPE_ATTRIBUTE} from "../../../meta/QAttribute";
import {QNUMBER} from "../../../meta/QDataType";
import {QType} from "../../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../../meta/QLabel";
import {HasPathXY} from "../HasPathXY";

export const TYPE_ROAD = QType({
  id: 'com.sevenq.type.where.geo.road',
  title: 'Road'
})


export const QRoad = stampit(
  HasPathXY,
  {
    name: TYPE_ROAD.title,
    deepStatics: {
      Type: TYPE_ROAD,
      MainLabel: TYPE_ROAD.title,
      Labels: [
        QLabel({
          id: [TYPE_LABEL.id,TYPE_ROAD.id].join('.'),
          title: TYPE_ROAD.title
        })
      ],
      Parents: [
        TYPE_ROAD
      ],
      Attributes: {},
    },
    init( {}, {stamp} ){
    }

  }
)
