import stampit from 'stampit'
import {QAttribute, TYPE_ATTRIBUTE} from "../../../meta/QAttribute";
import {QNUMBER} from "../../../meta/QDataType";
import {QType} from "../../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../../meta/QLabel";
import {HasPathXY} from "../HasPathXY";

export const TYPE_COUNTRY = QType({
  id: 'com.sevenq.type.where.geo.country',
  title: 'Country'
})


export const QCountry = stampit(
  HasPathXY,
  {
    name: TYPE_COUNTRY.title,
    deepStatics: {
      Type: TYPE_COUNTRY,
      MainLabel: TYPE_COUNTRY.title,
      Labels: [
        QLabel({
          id: [TYPE_LABEL.id,TYPE_COUNTRY.id].join('.'),
          title: TYPE_COUNTRY.title
        })
      ],
      Parents: [
        TYPE_COUNTRY
      ],
      Attributes: {},
    },
    init( {}, {stamp} ){
    }

  }
)
