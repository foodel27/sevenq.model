import stampit from 'stampit'
import {HasPositionXY} from "../HasPositionXY";
import {QAttribute, TYPE_ATTRIBUTE} from "../../../meta/QAttribute";
import {QNUMBER} from "../../../meta/QDataType";
import {QType} from "../../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../../meta/QLabel";

export const TYPE_VEHICLE = QType({
  id: 'com.sevenq.type.where.geo.vehicle',
  title: 'Vehicle'
})


export const QVehicle = stampit(
  HasPositionXY,
  {
    name: TYPE_VEHICLE.title,
    deepStatics: {
      Type: TYPE_VEHICLE,
      MainLabel: TYPE_VEHICLE.title,
      Labels: [
        QLabel({
          id: [TYPE_LABEL.id,TYPE_VEHICLE.id].join('.'),
          title: TYPE_VEHICLE.title
        })
      ],
      Parents: [
        TYPE_VEHICLE
      ],
      Attributes: {
        // Population : QAttribute({
        //   id: TYPE_ATTRIBUTE.id + "." + TYPE_VEHICLE.title + ".population",
        //   title: "Population",
        //   dataType: QNUMBER,
        //   required: false,
        //   fieldName: "population"})
      },
    },
    init( {}, {stamp} ){
    }

  }
)
