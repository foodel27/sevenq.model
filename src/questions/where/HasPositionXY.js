import uuid from 'uuid/v4'
import stampit from 'stampit'

import {QAttribute,TYPE_ATTRIBUTE} from "../../meta/QAttribute";
import {QNUMBER} from "../../meta/QDataType";
import {QWhere} from "./QWhere";

const TITLE_POSITION_XY = "PositionXY";


export const HasPositionXY = stampit(
  QWhere,
  {
	name: "HasPositionXY",
	deepStatics: {
		Attributes: {
			X : QAttribute({
				id: TYPE_ATTRIBUTE.id + "." + TITLE_POSITION_XY + ".x",
				title: "X",
				dataType: QNUMBER,
				required: true,
				fieldName: "x"}),

      Y : QAttribute({
        id: TYPE_ATTRIBUTE.id + "." + TITLE_POSITION_XY + ".y",
        title: "Y",
        dataType: QNUMBER,
        required: true,
        fieldName: "y"})
		},
	},
	init( {x,y}, {stamp} ){
		this[stamp.Attributes.X.fieldName] = x || 0
		this[stamp.Attributes.Y.fieldName] = y || 0
	}
})
