import stampit from 'stampit'
import {QWord} from "../../word/QWord";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_WHERE = QType({
	id: 'com.sevenq.type.where',
	title: 'Where'
})


export const QWhere = stampit(
	QWord,
	{
		name: TYPE_WHERE.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_WHERE.id,
					title: TYPE_WHERE.title
				})
			],
			Parents: [TYPE_WHERE],
			// Methods: {},
			// Attributes: {},
			// Relations: {},
		}
	}
)
