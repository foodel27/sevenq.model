import uuid from 'uuid/v4'
import stampit from 'stampit'

import {QAttribute,TYPE_ATTRIBUTE} from "../../meta/QAttribute";
import {QARRAY} from "../../meta/QDataType";
import {QWhere} from "./QWhere";

const TITLE_PATH_XY = "PathXY";


export const HasPathXY = stampit(
  QWhere,
  {
	name: "HasPathXY",
	deepStatics: {
		Attributes: {
      PathXY : QAttribute({
				id: TYPE_ATTRIBUTE.id + "." + TITLE_PATH_XY,
				title: "PathXY",
				dataType: QARRAY,
				required: true,
				fieldName: "pathXY"}),
		},
	},
	init( {pathXY}, {stamp} ){
		this[stamp.Attributes.PathXY.fieldName] = pathXY || [{x:0,y:0},{x:0,y:0}]
	}
})
