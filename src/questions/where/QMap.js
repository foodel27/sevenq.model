import uuid from 'uuid/v4'
import stampit from 'stampit'

import {QAttribute,TYPE_ATTRIBUTE} from "../../meta/QAttribute";
import {QARRAY} from "../../meta/QDataType";
import {QWhere} from "./QWhere";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";

export const TYPE_MAP = QType({
  id: 'com.sevenq.type.where.map',
  title: 'Map'
})


export const QMap = stampit(
  QWhere,
  {
	name: TYPE_MAP.title,
	deepStatics: {
    Type: TYPE_MAP,
	  MainLabel: TYPE_MAP.title,
    Labels: [
      QLabel({
        id: [TYPE_LABEL.id,TYPE_MAP.id].join('.'),
        title: TYPE_MAP.title
      })
    ],
    Parents: [
      TYPE_MAP
    ],
		Attributes: {
      Points : QAttribute({
				id: TYPE_ATTRIBUTE.id + "." + TYPE_MAP.title + ".points",
				title: "Points",
				dataType: QARRAY,
				required: false,
				fieldName: "points"}),
      Paths : QAttribute({
        id: TYPE_ATTRIBUTE.id + "." + TYPE_MAP.title + ".paths",
        title: "Paths",
        dataType: QARRAY,
        required: false,
        fieldName: "paths"}),
		},
	},
	init( {points, paths}, {stamp} ){
		this[stamp.Attributes.Points.fieldName] = points || []
		this[stamp.Attributes.Paths.fieldName] = paths || []
	}
})
