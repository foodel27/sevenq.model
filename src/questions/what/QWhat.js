import stampit from 'stampit'
import {QWord} from "../../word/QWord";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_WHAT = QType({
	id: 'com.sevenq.type.what',
	title: 'What'
})


export const QWhat = stampit(
	QWord,
	{
		name: TYPE_WHAT.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_WHAT.id,
					title: TYPE_WHAT.title
				})
			],
			Parents: [TYPE_WHAT],
			// Methods: {},
			// Attributes: {},
			// Relations: {},
		}
	}
)