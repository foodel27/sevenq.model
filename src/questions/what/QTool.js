import stampit from 'stampit'
import {QWhat} from "./QWhat";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_TOOL = QType({
	id: 'com.sevenq.type.what.tool',
	title: 'Tool'
})


export const QTool = stampit(
	QWhat,
	{
		name: TYPE_TOOL.title,
		deepStatics: {
      MainLabel: TYPE_TOOL.title,
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_TOOL.id,
					title: TYPE_TOOL.title
				})
			],
			Parents: [
				TYPE_TOOL
			],
			Methods: {},
			Attributes: {},
			Relations: {},
		}
	}
)
