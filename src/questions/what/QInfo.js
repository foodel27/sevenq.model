import stampit from 'stampit'
import {QWhat} from "./QWhat";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_INFO = QType({
	id: 'com.sevenq.type.what.info',
	title: 'Info'
})


export const QInfo = stampit(
	QWhat,
	{
		name: TYPE_INFO.title,
		deepStatics: {
      MainLabel: TYPE_INFO.title,
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_INFO.id,
					title: TYPE_INFO.title
				})
			],
			Parents: [
				TYPE_INFO
			],
			Methods: {},
			Attributes: {},
			Relations: {},
		}
	}
)
