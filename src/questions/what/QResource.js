import stampit from 'stampit'
import {QWhat} from "./QWhat";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_RESOURCE = QType({
	id: 'com.sevenq.type.what.resource',
	title: 'Resource'
})


export const QResource = stampit(
	QWhat,
	{
		name: TYPE_RESOURCE.title,
		deepStatics: {
		  MainLabel: TYPE_RESOURCE.title,
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_RESOURCE.id,
					title: TYPE_RESOURCE.title
				})
			],
			Parents: [
				TYPE_RESOURCE
			],
			Methods: {},
			Attributes: {},
			Relations: {},
		}
	}
)
