import stampit from 'stampit'

import * as dataTypes from '../../meta/QDataType'
import {QAttribute,TYPE_ATTRIBUTE} from "../../meta/QAttribute";
import {QEnum,ENUM_ID_PREFIX} from '../../meta/QEnum'

const TITLE_GENDER = "Gender";
const TITLE_MALE = 'Male';
const TITLE_FEMALE = 'Female';

export const ENUM_GENDER = QEnum(ENUM_ID_PREFIX + '.who.gender', TITLE_GENDER, [TITLE_MALE,TITLE_FEMALE])

export const HasGender = stampit({
  deepStatics: {
	  Attributes: {
		  Gender : QAttribute({
			  id: TYPE_ATTRIBUTE.id + "." + TITLE_GENDER,
			  title: TITLE_GENDER,
			  dataType: ENUM_GENDER,
			  required: true,
			  fieldName: TITLE_GENDER.toLowerCase()})
	  },
  },
  init({gender},{stamp}){
      // FIXME RANDOM INITIAL GENDER
    this[this.meta.Attributes.Gender.fieldName] = gender
      || (Math.floor(Math.random() * 2)) == 1 ? ENUM_GENDER.Male.title : ENUM_GENDER.Female.title
  }
})
