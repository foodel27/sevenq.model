import stampit from 'stampit'
import {QType} from "../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../word/QRelation";


const TYPE_MEMBER_OF = QType({
	id: [TYPE_RELATION.id,'who','memberOf'].join('.'),
	title: 'MemberOf'
})


export const QMemberOf = stampit(
	QRelation,
	{
		name: TYPE_MEMBER_OF.title,
		deepStatics: {
			Type: TYPE_MEMBER_OF,
			MainLabel: TYPE_MEMBER_OF.title,
			TargetTypes: ['Group'],
			SourceTypes: ['Role'],
			FieldName : "memberOf",
			Parents: [
				TYPE_MEMBER_OF
			],
			// Methods: {},
			// Attributes: {},
		},
    init({},{stamp}){
      this.title = TYPE_MEMBER_OF.title
      this.fieldName = this.meta.FieldName
    }

	}
)
