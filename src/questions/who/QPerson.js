import stampit from 'stampit'
import {QWho} from "./QWho";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";
import {HasFirstName} from "./HasFirstName";
import {HasGender} from "./HasGender";
import {QActsAs} from "./QActsAs";


const TYPE_PERSON = QType({
	id: 'com.sevenq.type.who.person',
	title: 'Person'
})


export const QPerson = stampit(
	QWho,
	HasFirstName,
	HasGender,
	{
		name: TYPE_PERSON.title,
		deepStatics: {
      MainLabel: TYPE_PERSON.title,
			Labels: [
				QLabel({
					id: [TYPE_LABEL.id,TYPE_PERSON.id].join('.'),
					title: TYPE_PERSON.title
				})
			],
			Parents: [
				TYPE_PERSON
			],
			// Methods: {},
			// Attributes: {},
			Relations: {
				ActsAs: QActsAs()
			},
		},
		init( {actsAs},{stamp} ){
			this[this.meta.Relations.ActsAs.fieldName] = actsAs || []
		}
	}
)