import stampit from 'stampit'
import {QType} from "../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../word/QRelation";


const TYPE_REPORTS_TO = QType({
	id: TYPE_RELATION.id + '.who.reportsTo',
	title: 'ReportsTo'
})


export const QReportsTo = stampit(
	QRelation,
	{
		name: TYPE_REPORTS_TO.title,
		deepStatics: {
			Type: TYPE_REPORTS_TO,
			MainLabel: TYPE_REPORTS_TO.title,
			TargetTypes: ['Role'],
			SourceTypes: ['Role'],
			FieldName : "reportsTo",
			Parents: [
				TYPE_REPORTS_TO
			],
			// Methods: {},
			// Attributes: {},
		},
    init({},{stamp}){
      this.title = TYPE_REPORTS_TO.title
      this.fieldName = this.meta.FieldName
    }

	}
)
