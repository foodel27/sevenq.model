import stampit from 'stampit'
import {QWord} from "../../word/QWord";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_WHO = QType({
	id: 'com.sevenq.type.who',
	title: 'Who'
})


export const QWho = stampit(
	QWord,
	{
		name: TYPE_WHO.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_WHO.id,
					title: TYPE_WHO.title
				})
			],
			Parents: [TYPE_WHO],
			// Methods: {},
			// Attributes: {},
			// Relations: {},
		}
	}
)