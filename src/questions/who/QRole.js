import stampit from 'stampit'
import {QWho} from "./QWho";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";
import {QReportsTo} from "./QReportsTo";
import {QMemberOf} from "./QMemberOf";


const TYPE_ROLE = QType({
	id: 'com.sevenq.type.who.role',
	title: 'Role'
})


export const QRole = stampit(
	QWho,
	{
		name: TYPE_ROLE.title,
		deepStatics: {
			Type: TYPE_ROLE,
			MainLabel: TYPE_ROLE.title,
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_ROLE.id,
					title: TYPE_ROLE.title
				})
			],
			Parents: [
				TYPE_ROLE
			],
			// Methods: {},
			// Attributes: {},
			Relations: {
				ReportsTo: QReportsTo(),
				MemberOf: QMemberOf(),
			},
		},
		init( {reportsTo, memberOf},{stamp} ){
			// console.dir(this)
			this[stamp.Relations.ReportsTo.fieldName] = reportsTo || []
			this[stamp.Relations.MemberOf.fieldName] = memberOf || []
		}
	}
)