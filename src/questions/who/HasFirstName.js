import uuid from 'uuid/v4'
import stampit from 'stampit'

import {QAttribute,TYPE_ATTRIBUTE} from "../../meta/QAttribute";
import {QSTRING} from "../../meta/QDataType";

const TITLE_FIRST_NAME = "FirstName";


export const HasFirstName = stampit({
	name: "HasFirstName",
	deepStatics: {
		Attributes: {
			FirstName : QAttribute({
				id: TYPE_ATTRIBUTE.id + "." + TITLE_FIRST_NAME,
				title: TITLE_FIRST_NAME,
				dataType: QSTRING,
				required: true,
				fieldName: "firstName"})
		},
	},
	init( {firstName}, {stamp} ){
		this[stamp.Attributes.FirstName.fieldName] = firstName
	}
})
