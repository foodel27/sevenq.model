import stampit from 'stampit'
import {QWho} from "./QWho";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_GROUP = QType({
	id: 'com.sevenq.type.who.group',
	title: 'Group'
})


export const QGroup = stampit(
	QWho,
	{
		name: TYPE_GROUP.title,
		deepStatics: {
      MainLabel: TYPE_GROUP.title,
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_GROUP.id,
					title: TYPE_GROUP.title
				})
			],
			Parents: [
				TYPE_GROUP
			],
			Methods: {},
			Attributes: {},
			Relations: {},
		}
	}
)