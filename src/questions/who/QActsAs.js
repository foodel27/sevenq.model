import stampit from 'stampit'
import {QType} from "../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../word/QRelation";

let TITLE_ACTS_AS = "actsAs";

const TYPE_ACTS_AS = QType({
	id: [TYPE_RELATION.id,'who',TITLE_ACTS_AS].join('.'),
	title: 'ActsAs'
})


export const QActsAs = stampit(
	QRelation,
	{
		name: TYPE_ACTS_AS.title,
		deepStatics: {
			Type: TYPE_ACTS_AS,
			MainLabel: TYPE_ACTS_AS.title,
			TargetTypes: ['Person'],
			SourceTypes: ['Role'],
			FieldName : TITLE_ACTS_AS,
			Parents: [
				TYPE_ACTS_AS
			],
			// Methods: {},
			// Attributes: {},
		},
    init({},{stamp}){
      this.title = TYPE_ACTS_AS.title
      this.fieldName = this.meta.FieldName
    }

	}
)
