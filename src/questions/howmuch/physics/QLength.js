import stampit from 'stampit'
import {QType} from "../../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../../meta/QLabel";
import {QAttribute, TYPE_ATTRIBUTE} from "../../../meta/QAttribute";
import {ENUM_ID_PREFIX, QEnum} from "../../../meta/QEnum";
import {QQuantity} from "../QQuantity";


const TYPE_LENGTH = QType({
	id: 'com.sevenq.type.howMuch.length',
	title: 'Length'
})

export const ENUM_LENGTH_UNITS = QEnum(
	ENUM_ID_PREFIX + '.howmuch.length.units',
	"LengthUnits",
	["cm","m","px","em"])

export const QLength = stampit(
	QQuantity,
	{

		name: TYPE_LENGTH.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: [TYPE_LABEL.id,TYPE_LENGTH.id].join('.'),
					title: TYPE_LENGTH.title
				})
			],
			Parents: [
				TYPE_LENGTH
			],
			Attributes: {
				Unit: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_LENGTH.id, "Unit"].join('.'),
					title: "Unit",
					dataType: ENUM_LENGTH_UNITS,
					required: true,
					fieldName: "unit"})
			},
			// Methods: {},
			// Relations: {},
		}
	}
)