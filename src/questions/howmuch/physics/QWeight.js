import stampit from 'stampit'
import {QType} from "../../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../../meta/QLabel";
import {QAttribute, TYPE_ATTRIBUTE} from "../../../meta/QAttribute";
import {ENUM_ID_PREFIX, QEnum} from "../../../meta/QEnum";
import {QQuantity} from "../QQuantity";


const TYPE_WEIGHT = QType({
	id: 'com.sevenq.type.howMuch.weight',
	title: 'Weight'
})

export const ENUM_WEIGHT_UNITS = QEnum(
	ENUM_ID_PREFIX + '.howmuch.weight.units',
	"WeightUnits",
	["kg","g","t","mg"])

export const QWeight = stampit(
	QQuantity,
	{

		name: TYPE_WEIGHT.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: [TYPE_LABEL.id,TYPE_WEIGHT.id].join('.'),
					title: TYPE_WEIGHT.title
				})
			],
			Parents: [
				TYPE_WEIGHT
			],
			Attributes: {
				Unit: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_WEIGHT.id, "Unit"].join('.'),
					title: "Unit",
					dataType: ENUM_WEIGHT_UNITS,
					required: true,
					fieldName: "unit"})
			},
			// Methods: {},
			// Relations: {},
		}
	}
)