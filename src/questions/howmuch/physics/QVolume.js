import stampit from 'stampit'
import {QType} from "../../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../../meta/QLabel";
import {QAttribute, TYPE_ATTRIBUTE} from "../../../meta/QAttribute";
import {ENUM_ID_PREFIX, QEnum} from "../../../meta/QEnum";
import {QQuantity} from "../QQuantity";


const TYPE_VOLUME = QType({
	id: 'com.sevenq.type.howMuch.volume',
	title: 'Volume'
})

export const ENUM_VOLUME_UNITS = QEnum(
	ENUM_ID_PREFIX + '.howmuch.volume.units',
	"VolumeUnits",
	["cc","Litre","Barrel", "Byte"])

export const QVolume = stampit(
	QQuantity,
	{

		name: TYPE_VOLUME.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: [TYPE_LABEL.id,TYPE_VOLUME.id].join('.'),
					title: TYPE_VOLUME.title
				})
			],
			Parents: [
				TYPE_VOLUME
			],
			Attributes: {
				Unit: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_VOLUME.id, "Unit"].join('.'),
					title: "Unit",
					dataType: ENUM_VOLUME_UNITS,
					required: true,
					fieldName: "unit"})
			},
			// Methods: {},
			// Relations: {},
		}
	}
)