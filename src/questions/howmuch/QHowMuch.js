import stampit from 'stampit'
import {QWord} from "../../word/QWord";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_HOWMUCH = QType({
	id: 'com.sevenq.type.howMuch',
	title: 'HowMuch'
})


export const QHowMuch = stampit(
	QWord,
	{
		name: TYPE_HOWMUCH.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: [TYPE_LABEL.id,TYPE_HOWMUCH.id].join('.'),
					title: TYPE_HOWMUCH.title
				})
			],
			Parents: [TYPE_HOWMUCH],
			// Methods: {},
			// Attributes: {},
			// Relations: {},
		}
	}
)