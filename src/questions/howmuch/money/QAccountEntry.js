import stampit from 'stampit'
import {QType} from "../../../meta/QType";
import {QHowMuch} from "../QHowMuch";
import {QLabel, TYPE_LABEL} from "../../../meta/QLabel";
import {QAttribute, TYPE_ATTRIBUTE} from "../../../meta/QAttribute";
import {QDATE, QNUMBER, QOBJECT, QSTRING} from "../../../meta/QDataType";
import {QMoney} from "./QMoney";



export const TYPE_ACCOUNT_ENTRY = QType({
	id: 'com.sevenq.type.howMuch.accountEntry',
	title: 'AccountEntry'
})

export const QAccountEntry = stampit(
	QHowMuch,
	{

		name: TYPE_ACCOUNT_ENTRY.title,
		deepStatics: {
			Type: TYPE_ACCOUNT_ENTRY,
			MainLabel: TYPE_ACCOUNT_ENTRY.title,
			Labels: [
				QLabel({
					id: [TYPE_LABEL.id,TYPE_ACCOUNT_ENTRY.id].join('.'),
					title: TYPE_ACCOUNT_ENTRY.title
				})
			],
			Parents: [
				TYPE_ACCOUNT_ENTRY
			],
			Attributes: {
				Amount: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_ACCOUNT_ENTRY.id, "Amount"].join('.'),
					title: "Amount",
					dataType: QMoney,
					required: true,
					fieldName: "amount"}),
				Description: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_ACCOUNT_ENTRY.id, "Description"].join('.'),
					title: "Description",
					dataType: QSTRING,
					required: true,
					fieldName: "description"}),
				AccountFrom: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_ACCOUNT_ENTRY.id, "AccountFrom"].join('.'),
					title: "AccountFrom",
					dataType: QOBJECT,
					required: true,
					fieldName: "accountFrom"}),
				AccountTo: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_ACCOUNT_ENTRY.id, "AccountTo"].join('.'),
					title: "AccountTo",
					dataType: QOBJECT,
					required: true,
					fieldName: "accountTo"}),
				CreatedOn: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_ACCOUNT_ENTRY.id, "CreatedOn"].join('.'),
					title: "CreatedOn",
					dataType: QDATE,
					required: true,
					fieldName: "createdOn"}),
				ReconciledOn: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_ACCOUNT_ENTRY.id, "ReconciledOn"].join('.'),
					title: "ReconciledOn",
					dataType: QDATE,
					required: true,
					fieldName: "reconciledOn"}),
			}
		},
		// Relations: {},
		// Methods: {},

		init( {amount,description,accountFrom,accountTo},{stamp} ){
			this[this.meta.Attributes.Amount.fieldName] = amount
			this[this.meta.Attributes.Description.fieldName] = description
			this[this.meta.Attributes.AccountFrom.fieldName] = accountFrom
			this[this.meta.Attributes.AccountTo.fieldName] = accountTo
			this[this.meta.Attributes.CreatedOn.fieldName] = new Date().getTime()
		},

	}
)