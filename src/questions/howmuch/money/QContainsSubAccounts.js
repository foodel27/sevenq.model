import stampit from 'stampit'
import {QType} from "../../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../../word/QRelation";


let TITLE_CONTAINS_SUB_ACCOUNTS = "containsSubAccounts";

const TYPE_CONTAINS_SUB_ACCOUNTS = QType({
	id: [TYPE_RELATION.id,'howmuch',TITLE_CONTAINS_SUB_ACCOUNTS].join('.'),
	title: 'ContainsSubAccounts'
})


export const QContainsSubAccounts = stampit(
	QRelation,
	{
		name: TYPE_CONTAINS_SUB_ACCOUNTS.title,
		deepStatics: {
			Type: TYPE_CONTAINS_SUB_ACCOUNTS,
			MainLabel: TYPE_CONTAINS_SUB_ACCOUNTS.title,
			TargetTypes: ['Account'],
			SourceTypes: ['Account'],
			FieldName : TITLE_CONTAINS_SUB_ACCOUNTS,
			Parents: [
				// FIXME extend RelationContains?
				TYPE_CONTAINS_SUB_ACCOUNTS
			],
			// Methods: {},
			// Attributes: {},
		},
		init(){
			this.title = TITLE_CONTAINS_SUB_ACCOUNTS
			this.fieldName = this.meta.FieldName
		}
	}
)