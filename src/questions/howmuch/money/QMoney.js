import stampit from 'stampit'
import {QType} from "../../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../../meta/QLabel";
import {QAttribute, TYPE_ATTRIBUTE} from "../../../meta/QAttribute";
import {ENUM_ID_PREFIX, QEnum} from "../../../meta/QEnum";
import {QQuantity} from "../QQuantity";
import {QENUM} from "../../../meta/QDataType";


export const TYPE_MONEY = QType({
	id: 'com.sevenq.type.howMuch.money',
	title: 'Money'
})

export const ENUM_MONEY_CURRENCIES = QEnum(
	ENUM_ID_PREFIX + '.howmuch.money.currencies',
	"Currencies",
	["USD","NIS","EUR","GBP","JPY"])

export const QMoney = stampit(
	QQuantity,
	{

		name: TYPE_MONEY.title,
		deepStatics: {
			Type: TYPE_MONEY,
			MainLabel: TYPE_MONEY.title,
			Labels: [
				QLabel({
					id: [TYPE_LABEL.id,TYPE_MONEY.id].join('.'),
					title: TYPE_MONEY.title
				})
			],
			Parents: [
				TYPE_MONEY
			],
			Attributes: {
				// FIXME rename to CURRENCY or keep UNIT for consistency???
				Unit: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_MONEY.id, "Unit"].join('.'),
					title: "Unit",
					dataType: QENUM,
          // FIXME refactor from full blown Attr "UnitEnum"
					// dataTypeRef: ENUM_MONEY_CURRENCIES,
					required: true,
					fieldName: "unit"}),
        UnitEnum: ENUM_MONEY_CURRENCIES
			},
			// Methods: {},
			// Relations: {},
		},
		init( {unit}, {stamp}){
			this[stamp.Attributes.Unit.fieldName] = unit
		}
	}
)
