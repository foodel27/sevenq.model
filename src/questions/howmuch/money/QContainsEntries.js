import stampit from 'stampit'
import {QType} from "../../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../../word/QRelation";


let TITLE_CONTAINS_ENTRIES = "containsEntries";

const TYPE_CONTAINS_ENTRIES = QType({
	id: [TYPE_RELATION.id,'howmuch',TITLE_CONTAINS_ENTRIES].join('.'),
	title: 'ContainsEntries'
})


export const QContainsEntries = stampit(
	QRelation,
	{
		name: TYPE_CONTAINS_ENTRIES.title,
		deepStatics: {
			Type: TYPE_CONTAINS_ENTRIES,
			MainLabel: TYPE_CONTAINS_ENTRIES.title,
			TargetTypes: ['AccountEntry'],
			SourceTypes: ['Account'],
			FieldName : TITLE_CONTAINS_ENTRIES,
			Parents: [
				// FIXME extend RelationContains?
				TYPE_CONTAINS_ENTRIES
			],
			// Methods: {},
			// Attributes: {},
		},
		init(){
			this.title = TITLE_CONTAINS_ENTRIES
			this.fieldName = this.meta.FieldName
		}
	}
)