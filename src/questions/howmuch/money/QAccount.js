import stampit from 'stampit'
import {QHowMuch} from "../QHowMuch";
import {QType} from "../../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../../meta/QLabel";
import {QAttribute, TYPE_ATTRIBUTE} from "../../../meta/QAttribute";
import {QOBJECT} from "../../../meta/QDataType";
import {ENUM_MONEY_CURRENCIES, QMoney} from "./QMoney";
import {QMethod} from "../../../meta/QMethod";
import {QParameter} from "../../../meta/QParameter";
import {QEvent} from "../../../flow/QEvent";
import {QAccountEntry} from "./QAccountEntry";
import {QContainsEntries} from "./QContainsEntries";
import {QContainsSubAccounts} from "./QContainsSubAccounts";
import {QEventBus} from "../../../flow/QEventBus";


export const TYPE_ACCOUNT = QType({
	id: 'com.sevenq.type.howMuch.account',
	title: 'Account'
})

export const QParameter_Account_AddEntry_Entry = stampit(
	QParameter,
	{
		name: TYPE_ACCOUNT.title + ".AddEntry.Entry",
		init(){
			this.title = TYPE_ACCOUNT.title + '#AddEntry.Entry'
			this.dataType = QAccountEntry
			this.required = true
			this.fieldName = 'entry'
			this.description = "Add this Entry to this Account"
		}
	}
)

export const QParameter_Account_AddSubAccount_SubAccount = stampit(
	QParameter,
	{
		name: TYPE_ACCOUNT.title + ".AddSubAccount.SubAccount",
		init(){
			this.title = TYPE_ACCOUNT.title + '#AddSubAccount.SubAccount'
			this.dataType = QAccount
			this.required = true
			this.fieldName = 'subAccount'
			this.description = "Add this SubAccount to this Account"
		}
	}
)

export const QEvent_Account_SubAccountAdded = stampit(
	QEvent,
	{
		name: TYPE_ACCOUNT.title + ".SubAccountAdded",
		init({}, {stamp}) {
			this.title = TYPE_ACCOUNT.title + '.SubAccountAdded'
		},
		// methods: {
		// 	apply(){
		// 		let account = this.target
		// 		let entry = this.payload
		// 		// FIXME throw on irrelevant entry?
		// 		if(entry.accountFrom.id != account.id && entry.accountTo.id != account.id)
		// 			return
		// 		let isIncoming = entry.accountTo.id == account.id
		//
		// 		let existingEntries = account[account.meta.Relations.ContainsEntries.fieldName]
		// 		account[account.meta.Relations.ContainsEntries.fieldName] = [...existingEntries, entry]
		//
		// 		let existingBalance = account[account.meta.Attributes.Balance.fieldName]
		// 		account[account.meta.Attributes.Balance.fieldName] = QMoney({
		// 			amount: isIncoming ?
		// 				existingBalance.amount + entry.amount.amount
		// 				:
		// 				existingBalance.amount - entry.amount.amount,
		// 			unit: account[account.meta.Attributes.Currency.fieldName]
		// 		})
		// 	}
		// }
	}
)

export const QEvent_Account_EntryAdded = stampit(
	QEvent,
	{
		name: TYPE_ACCOUNT.title + ".EntryAdded",
		init({},{stamp}){
			this.title = TYPE_ACCOUNT.title + '.EntryAdded'
		},
		// methods: {
		// 	apply(){
		// 		let account = this.target
		// 		let entry = this.payload
		// 		// FIXME throw on irrelevant entry?
		// 		if(entry.accountFrom.id != account.id && entry.accountTo.id != account.id)
		// 			return
		// 		let isIncoming = entry.accountTo.id == account.id
		//
		// 		let existingEntries = account[account.meta.Relations.ContainsEntries.fieldName]
		// 		account[account.meta.Relations.ContainsEntries.fieldName] = [...existingEntries, entry]
		//
		// 		let existingBalance = account[account.meta.Attributes.Balance.fieldName]
		// 		account[account.meta.Attributes.Balance.fieldName] = QMoney({
		// 			amount: isIncoming ?
		// 				existingBalance.amount + entry.amount.amount
		// 				:
		// 				existingBalance.amount - entry.amount.amount,
		// 			unit: account[account.meta.Attributes.Currency.fieldName]
		// 		})
		// 	}
		// }
	}
)

export const QAccount = stampit(
	QHowMuch,
	{

		name: TYPE_ACCOUNT.title,
		deepStatics: {
			Type: TYPE_ACCOUNT,
			MainLabel: TYPE_ACCOUNT.title,
			Labels: [
				QLabel({
					id: [TYPE_LABEL.id,TYPE_ACCOUNT.id].join('.'),
					title: TYPE_ACCOUNT.title
				})
			],
			Parents: [
				TYPE_ACCOUNT
			],
			Attributes: {
				Currency: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_ACCOUNT.id, "Currency"].join('.'),
					title: "Currency",
					dataType: ENUM_MONEY_CURRENCIES,
					required: true,
					defaultValue: ENUM_MONEY_CURRENCIES.USD,
					fieldName: "currency"}),
				InitialBalance: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_ACCOUNT.id, "InitialBalance"].join('.'),
					title: "InitialBalance",
					dataType: QMoney,
					required: true,
					fieldName: "initialBalance"}),
				Balance: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_ACCOUNT.id, "Balance"].join('.'),
					title: "Balance",
					dataType: QMoney,
					required: true,
					readOnly: true,
					fieldName: "balance"}),
				ParentAccount: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_ACCOUNT.id, "ParentAccount"].join('.'),
					title: "ParentAccount",
					dataType: QOBJECT,
					fieldName: "parentAccount"}),
			},
			Relations: {
				ContainsEntries: QContainsEntries(),
				ContainsSubAccounts: QContainsSubAccounts(),
			},
			Methods: {
				AddEntry: QMethod({
					title : "AddEntry",
					fieldName : "addEntry",
					description : "Add this Entry to this Account and re-calculate the Account.Balance",
					parametersIn : [
						QParameter_Account_AddEntry_Entry
					],
					// ParameterOut : { type: QParameter },
					emitsEvents : [
						QEvent_Account_EntryAdded
					],
				}),
				AddSubAccount: QMethod({
					title : "AddSubAccount",
					fieldName : "addSubAccount",
					description : "Add the SubAccount to this Account and re-calculate the Account.Balance",
					parametersIn : [
						QParameter_Account_AddSubAccount_SubAccount
					],
					// ParameterOut : { type: QParameter },
					emitsEvents : [
						QEvent_Account_SubAccountAdded
					],
				})
			}

		},
		init( {currency, initialBalance,containsEntries,containsSubAccounts,parentAccount} ){

			this[this.meta.Attributes.Currency.fieldName] = currency
				|| this.meta.Attributes.Currency.defaultValue

			this[this.meta.Attributes.InitialBalance.fieldName] = QMoney({
				amount: initialBalance || 0,
				unit: this[this.meta.Attributes.Currency.fieldName]
			})

			this[this.meta.Attributes.Balance.fieldName] = QMoney({
				amount: initialBalance || 0,
				unit: this[this.meta.Attributes.Currency.fieldName]
			})

			this[this.meta.Relations.ContainsEntries.fieldName] = containsEntries || []
			this[this.meta.Relations.ContainsSubAccounts.fieldName] = containsSubAccounts || []
			this[this.meta.Attributes.ParentAccount.fieldName] = parentAccount
		},
		methods: {
			calculateBalance() {
				let account = this
				let totalEntries = 0
				let totalSubAccounts = 0
				let initialBalance = this[this.meta.Attributes.InitialBalance.fieldName].amount
				let existingBalance = account[account.meta.Attributes.Balance.fieldName].amount
				for(let entry of this[this.meta.Relations.ContainsEntries.fieldName]){

					let isIncoming = entry.accountTo.id == account.id
						if(isIncoming)
							totalEntries += entry.amount.amount
							else
							totalEntries-= entry.amount.amount

				}

				for(let subAccount of this[this.meta.Relations.ContainsSubAccounts.fieldName]){
					totalSubAccounts += subAccount.balance.amount
				}

				let newTotal = totalEntries + totalSubAccounts + initialBalance
				if(existingBalance != newTotal){
					this[this.meta.Attributes.Balance.fieldName] = QMoney({
						amount: newTotal,
						unit: this[this.meta.Attributes.Currency.fieldName]
					})
					if(this[this.meta.Attributes.ParentAccount.fieldName])
						this[this.meta.Attributes.ParentAccount.fieldName].calculateBalance()
				}

			},
			addEntry(entry){

				let account = this
				// let entry = this.payload
				// FIXME throw on irrelevant entry?
				if(entry.accountFrom.id != account.id && entry.accountTo.id != account.id)
					return


				let existingEntries = account[account.meta.Relations.ContainsEntries.fieldName]
				account[account.meta.Relations.ContainsEntries.fieldName] = [...existingEntries, entry]

				this.calculateBalance();

				// FIXME event-handling should be external
				QEventBus.emit(QEvent_Account_EntryAdded({
					target: this,
					payload: entry
				}))
			},

			addSubAccount(subAccount){
				let account = this
				let existingSubAccounts = this[this.meta.Relations.ContainsSubAccounts.fieldName]
				this[this.meta.Relations.ContainsSubAccounts.fieldName] =
					[...existingSubAccounts, subAccount]
				if(subAccount[this.meta.Attributes.ParentAccount.fieldName])
					subAccount[this.meta.Attributes.ParentAccount.fieldName].removeSubAccount(subAccount)
				subAccount[this.meta.Attributes.ParentAccount.fieldName] = account
				this.calculateBalance()
			},
			removeSubAccount(subAccount){
				let existingSubAccounts = this[this.meta.Relations.ContainsSubAccounts.fieldName]
				this[this.meta.Relations.ContainsSubAccounts.fieldName] =
					existingSubAccounts.filter( subAcc => subAcc.id != subAccount.id)
				this.calculateBalance()
			}
		}
	}

)


