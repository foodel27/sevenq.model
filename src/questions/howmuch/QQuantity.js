import stampit from 'stampit'
import {QHowMuch} from "./QHowMuch";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";
import {QAttribute, TYPE_ATTRIBUTE} from "../../meta/QAttribute";
import {QNUMBER} from "../../meta/QDataType";


const TYPE_QUANTITY = QType({
	id: 'com.sevenq.type.howMuch.quantity',
	title: 'Quantity'
})

export const QQuantity = stampit(
	QHowMuch,
	{

		name: TYPE_QUANTITY.title,
		deepStatics: {
			Type: TYPE_QUANTITY,
			MainLabel: TYPE_QUANTITY.title,
			Labels: [
				QLabel({
					id: [TYPE_LABEL.id,TYPE_QUANTITY.id].join('.'),
					title: TYPE_QUANTITY.title
				})
			],
			Parents: [
				TYPE_QUANTITY
			],
			// Methods: {},
			Attributes: {
				Amount: QAttribute({
					id: [TYPE_ATTRIBUTE.id, TYPE_QUANTITY.id, "Amount"].join('.'),
					title: "Amount",
					dataType: QNUMBER,
					required: true,
					fieldName: "amount"}),

			},
			// Relations: {},
		},
		init( {amount}, {stamp}){
			this[stamp.Attributes.Amount.fieldName] = amount || 0
		}
	}
)