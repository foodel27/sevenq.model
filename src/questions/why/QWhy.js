import stampit from 'stampit'
import {QWord} from "../../word/QWord";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_WHY = QType({
	id: 'com.sevenq.type.why',
	title: 'Why'
})


export const QWhy = stampit(
	QWord,
	{
		name: TYPE_WHY.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_WHY.id,
					title: TYPE_WHY.title
				})
			],
			Parents: [TYPE_WHY],
			// Methods: {},
			// Attributes: {},
			// Relations: {},
		}
	}
)