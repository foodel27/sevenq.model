import stampit from 'stampit'
import {QWhy} from "./QWhy";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_CAUSE = QType({
	id: 'com.sevenq.type.why.cause',
	title: 'Cause'
})


export const QCause = stampit(
	QWhy,
	{
		name: TYPE_CAUSE.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_CAUSE.id,
					title: TYPE_CAUSE.title
				})
			],
			Parents: [
				TYPE_CAUSE
			],
			Methods: {},
			Attributes: {},
			Relations: {},
		}
	}
)