import stampit from 'stampit'
import {QWhy} from "./QWhy";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";
import {QCausedBy} from "./QCausedBy";


const TYPE_EFFECT = QType({
	id: 'com.sevenq.type.why.effect',
	title: 'Effect'
})


export const QEffect = stampit(
	QWhy,
	{
		name: TYPE_EFFECT.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_EFFECT.id,
					title: TYPE_EFFECT.title
				})
			],
			Parents: [
				TYPE_EFFECT
			],
			Methods: {},
			Attributes: {},
			Relations: {
				CausedBy: QCausedBy()
			},
		},
		init({causedBy},{stamp}) {
			this[this.meta.Relations.CausedBy.fieldName] = causedBy || []
		}
	}

)