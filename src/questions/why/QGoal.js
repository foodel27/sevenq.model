import stampit from 'stampit'
import {QWhy} from "./QWhy";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_GOAL = QType({
	id: 'com.sevenq.type.why.goal',
	title: 'Goal'
})


export const QGoal = stampit(
	QWhy,
	{
		name: TYPE_GOAL.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_GOAL.id,
					title: TYPE_GOAL.title
				})
			],
			Parents: [
				TYPE_GOAL
			],
			Methods: {},
			Attributes: {},
			Relations: {},
		}
	}
)