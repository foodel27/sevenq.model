import stampit from 'stampit'
import {QType} from "../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../word/QRelation";


const TYPE_CAUSED_BY = QType({
	id: [TYPE_RELATION.id,'why','causedBy'].join('.'),
	title: 'CausedBy'
})


export const QCausedBy = stampit(
	QRelation,
	{
		name: TYPE_CAUSED_BY.title,
		deepStatics: {
			Type: TYPE_CAUSED_BY,
			MainLabel: TYPE_CAUSED_BY.title,
			TargetTypes: ['Cause'],
			SourceTypes: ['Effect'],
			FieldName : "causedBy",
			Parents: [
				TYPE_CAUSED_BY
			],
			// Methods: {},
			// Attributes: {},
		},
		init({},{stamp}){
			this.title = TYPE_CAUSED_BY.title
			this.fieldName = this.meta.FieldName
		}

	}
)