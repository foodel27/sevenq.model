import stampit from 'stampit'
import {QWhy} from "./QWhy";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_PROBLEM = QType({
	id: 'com.sevenq.type.why.problem',
	title: 'Problem'
})


export const QProblem = stampit(
	QWhy,
	{
		name: TYPE_PROBLEM.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_PROBLEM.id,
					title: TYPE_PROBLEM.title
				})
			],
			Parents: [
				TYPE_PROBLEM
			],
			Methods: {},
			Attributes: {},
			Relations: {},
		}
	}
)