import stampit from 'stampit'
import {QType} from "../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../word/QRelation";


const TYPE_USES_TOOL = QType({
	id: [TYPE_RELATION.id,'how','usesTool'].join('.'),
	title: 'UsesTool'
})


export const QUsesTool = stampit(
	QRelation,
	{
		name: TYPE_USES_TOOL.title,
		deepStatics: {
			Type: TYPE_USES_TOOL,
			MainLabel: TYPE_USES_TOOL.title,
			TargetTypes: ['Tool'],
			SourceTypes: ['Task'],
			FieldName : "usesTool",
			Parents: [
				TYPE_USES_TOOL
			],
			// Methods: {},
			// Attributes: {},
		},
    init({},{stamp}){
      this.title = TYPE_USES_TOOL.title
      this.fieldName = this.meta.FieldName
    }

	}
)
