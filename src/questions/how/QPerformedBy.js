import stampit from 'stampit'
import {QType} from "../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../word/QRelation";


const TYPE_PERFORMED_BY = QType({
	id: [TYPE_RELATION.id,'how','performedBy'].join('.'),
	title: 'PerformedBy'
})


export const QPerformedBy = stampit(
	QRelation,
	{
		name: TYPE_PERFORMED_BY.title,
		deepStatics: {
			Type: TYPE_PERFORMED_BY,
			MainLabel: TYPE_PERFORMED_BY.title,
			TargetTypes: ['Role'],
			SourceTypes: ['Task'],
			FieldName : "performedBy",
			Parents: [
				TYPE_PERFORMED_BY
			],
			// Methods: {},
			// Attributes: {},
		},
    init({},{stamp}){
      this.title = TYPE_PERFORMED_BY.title
      this.fieldName = this.meta.FieldName
    }

	}
)
