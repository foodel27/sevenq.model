import stampit from 'stampit'
import {QWord} from "../../word/QWord";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";


const TYPE_HOW = QType({
	id: 'com.sevenq.type.how',
	title: 'How'
})


export const QHow = stampit(
	QWord,
	{
		name: TYPE_HOW.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_HOW.id,
					title: TYPE_HOW.title
				})
			],
			Parents: [TYPE_HOW],
			// Methods: {},
			// Attributes: {},
			// Relations: {},
		}
	}
)