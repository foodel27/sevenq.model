import stampit from 'stampit'
import {QType} from "../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../word/QRelation";


const TYPE_DEPENDS_ON = QType({
	id: [TYPE_RELATION.id,'how','dependsOn'].join('.'),
	title: 'DependsOn'
})


export const QDependsOn = stampit(
	QRelation,
	{
		name: TYPE_DEPENDS_ON.title,
		deepStatics: {
			Type: TYPE_DEPENDS_ON,
			MainLabel: TYPE_DEPENDS_ON.title,
			TargetTypes: ['Task'],
			SourceTypes: ['Task'],
			FieldName : "dependsOn",
			Parents: [
				TYPE_DEPENDS_ON
			],
			// Methods: {},
			// Attributes: {},
		},
    init({},{stamp}){
      this.title = TYPE_DEPENDS_ON.title
      this.fieldName = this.meta.FieldName
    }

	}
)
