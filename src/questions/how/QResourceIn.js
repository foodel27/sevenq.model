import stampit from 'stampit'
import {QType} from "../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../word/QRelation";


const TYPE_RESOURCE_IN = QType({
	id: [TYPE_RELATION.id,'how','resourceIn'].join('.'),
	title: 'ResourceIn'
})


export const QResourceIn = stampit(
	QRelation,
	{
		name: TYPE_RESOURCE_IN.title,
		deepStatics: {
			Type: TYPE_RESOURCE_IN,
			MainLabel: TYPE_RESOURCE_IN.title,
			TargetTypes: ['Resource'],
			SourceTypes: ['Task'],
			FieldName : "resourceIn",
			Parents: [
				TYPE_RESOURCE_IN
			],
			// Methods: {},
			// Attributes: {},
		},
    init({},{stamp}){
      this.title = TYPE_RESOURCE_IN.title
      this.fieldName = this.meta.FieldName
    }

	}
)
