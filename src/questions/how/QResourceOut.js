import stampit from 'stampit'
import {QType} from "../../meta/QType";
import {QRelation,TYPE_RELATION} from "../../word/QRelation";


const TYPE_RESOURCE_OUT = QType({
	id: [TYPE_RELATION.id,'how','resourceOut'].join('.'),
	title: 'ResourceOut'
})


export const QResourceOut = stampit(
	QRelation,
	{
		name: TYPE_RESOURCE_OUT.title,
		deepStatics: {
			Type: TYPE_RESOURCE_OUT,
			MainLabel: TYPE_RESOURCE_OUT.title,
			TargetTypes: ['Resource'],
			SourceTypes: ['Task'],
			FieldName : "resourceOut",
			Parents: [
				TYPE_RESOURCE_OUT
			],
			// Methods: {},
			// Attributes: {},
		},
    init({},{stamp}){
      this.title = TYPE_RESOURCE_OUT.title
      this.fieldName = this.meta.FieldName
    }

	}
)
