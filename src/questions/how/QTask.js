import stampit from 'stampit'
import {QHow} from "./QHow";
import {QType} from "../../meta/QType";
import {QLabel, TYPE_LABEL} from "../../meta/QLabel";
import {QUsesTool} from "./QUsesTool";
import {QPerformedBy} from "./QPerformedBy";
import {QResourceIn} from "./QResourceIn";
import {QResourceOut} from "./QResourceOut";
import {QDependsOn} from "./QDependsOn";


const TYPE_TASK = QType({
	id: 'com.sevenq.type.how.task',
	title: 'Task'
})


export const QTask = stampit(
	QHow,
	{
		name: TYPE_TASK.title,
		deepStatics: {
			Labels: [
				QLabel({
					id: TYPE_LABEL.id + "." + TYPE_TASK.id,
					title: TYPE_TASK.title
				})
			],
			Parents: [
				TYPE_TASK
			],
			Methods: {},
			Attributes: {},
			Relations: {
				UsesTool: QUsesTool(),
				PerformedBy: QPerformedBy(),
				ResourceIn: QResourceIn(),
				ResourceOut: QResourceOut(),
				DependsOn: QDependsOn(),
				// TODO TriggersTask ???
			},
		},
		init( {usesTool,performedBy,resourceIn,resourceOut,dependsOn}, {stamp}){
			this[this.meta.Relations.UsesTool.fieldName] = usesTool || []
			this[this.meta.Relations.PerformedBy.fieldName] = performedBy || []
			this[this.meta.Relations.ResourceIn.fieldName] = resourceIn || []
			this[this.meta.Relations.ResourceOut.fieldName] = resourceOut || []
			this[this.meta.Relations.DependsOn.fieldName] = dependsOn || []
		}
	}
)