import uuid from 'uuid/v4'
import stampit from 'stampit'
import {QType} from './QType'
import {QSTRING,QBOOLEAN,QOBJECT} from "./QDataType";

export const TYPE_ATTRIBUTE = QType({
	id: 'com.sevenq.type.attribute',
	title: 'Attribute'
})

export const QAttribute = stampit({
	name: TYPE_ATTRIBUTE.title,
	deepStatics: {
		MainLabel: TYPE_ATTRIBUTE.title,
		Type: TYPE_ATTRIBUTE,
		Attributes: {
			Id : { type: QSTRING },
			Title : { type: QSTRING },
			DataType : { type: QSTRING },
			Required : { type: QBOOLEAN },
			DefaultValue : { type: QOBJECT },
			FieldName : { type: QSTRING },
			Description : { type: QSTRING },
			ReadOnly: {type: QBOOLEAN}
		}
	},
	init( {id, title, dataType, required, defaultValue, fieldName, description, readOnly}, {stamp}){
		this.meta = stamp
		this.id = id || uuid()
		this.title = title
		this.dataType = dataType
		this.required = !!required
		this.defaultValue = defaultValue
		this.fieldName = fieldName
		this.description = description
		this.readOnly = readOnly
	},
})
