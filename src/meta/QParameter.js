import uuid from 'uuid/v4'
import stampit from 'stampit'
import {QType} from './QType'
import {QAttribute} from "./QAttribute";

export const TYPE_PARAMETER = QType({
	id: 'com.sevenq.type.parameter',
	title: 'Parameter'
})

export const QParameter = stampit(
	QAttribute,
	{
		name: TYPE_PARAMETER.title,
		deepStatics: {
			MainLabel: TYPE_PARAMETER.title,
			Type: TYPE_PARAMETER,
		},
	})
