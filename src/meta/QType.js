import stampit from 'stampit'
import uuid from 'uuid/v4'
import {QSTRING} from "./QDataType";

export const QType = stampit({
	name: "Type",
	deepStatics: {
		MainLabel: "Type",
		Attributes: {
			Id: {type: QSTRING},
			Title: {type: QSTRING},
		},
	},
	init( {id, title},{stamp}){
		this.meta = stamp
		this.id = id || uuid()
		this.title = title
	}
})
