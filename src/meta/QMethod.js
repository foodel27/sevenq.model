import uuid from 'uuid/v4'
import stampit from 'stampit'
import {QType} from './QType'
import {QSTRING,QARRAY} from "./QDataType";
import {QParameter} from "./QParameter";

export const TYPE_METHOD = QType({
	id: 'com.sevenq.type.method',
	title: 'Method'
})

export const QMethod = stampit(
	{
	name: TYPE_METHOD.title,
	deepStatics: {
		MainLabel: TYPE_METHOD.title,
		Type: TYPE_METHOD,
		Attributes: {
			Id : { type: QSTRING },
			Title : { type: QSTRING },
			FieldName : { type: QSTRING },
			Description : { type: QSTRING },
			ParametersIn : { type: QARRAY },
			ParameterOut : { type: QParameter },
			EmitsEvents : { type: QARRAY },
		}
	},
	init( {id, title, fieldName, description, parametersIn, parameterOut, emitsEvents}, {stamp}){
		this.meta = stamp
		this.id = id || uuid()
		this.title = title
		this.fieldName = fieldName
		this.description = description
		this.parametersIn = parametersIn || []
		this.parameterOut = parameterOut
		this.emitsEvents = emitsEvents
	},
	methods: {
		run(methodOwner, args){
			try {
				return methodOwner[this.fieldName](args)
			} catch (e) {
				console.error(this.title + ': evaluation failed: ' + e)
			}
		}
	}
})
