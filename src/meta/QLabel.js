import stampit from 'stampit'
import uuid from 'uuid/v4'
import {QType} from "./QType"
import {QSTRING} from "./QDataType"

export const TYPE_LABEL = QType({
	id: 'com.sevenq.type.label',
	title: 'Label'
})

export const QLabel = stampit({
	name: TYPE_LABEL.title,
	deepStatics: {
		MainLabel: TYPE_LABEL.title,
		Type: TYPE_LABEL,
		Attributes: {
			Id: {type: QSTRING},
			Title: {type: QSTRING},
		},
	},
	init({id, title},{ stamp }) {
		this.meta = stamp
		this.id = id || uuid()
		this.title = title
	},
})