import stampit from 'stampit'

const DATA_TYPE_PREFIX = 'com.sevenq.datatype'

const QDatatype = stampit(
	{
		name: "DataType",
		deepStatics: {
			MainLabel : "DataType",
      // FIXME circular dependency QType <=> QDataType
      Attributes: {
        Id: {type: 'String'},
        Title: {type: 'String'},
      },
		},
		init( {id, title}, {stamp}){
			this.meta = stamp
			this.id = id
			this.title = title
		}
	})

export const QSTRING = QDatatype({id: DATA_TYPE_PREFIX + '.String', title: 'String'})
export const QNUMBER = QDatatype({id: DATA_TYPE_PREFIX + '.Number', title: 'Number'})
export const QBOOLEAN = QDatatype({id: DATA_TYPE_PREFIX + '.Boolean', title: 'Boolean'})
export const QDATE = QDatatype({id: DATA_TYPE_PREFIX + '.Date', title: 'Date'})
export const QOBJECT = QDatatype({id: DATA_TYPE_PREFIX + '.Object', title: 'Object'})
export const QARRAY = QDatatype({id: DATA_TYPE_PREFIX + '.Array', title: 'Array'})
export const QFUNCTION = QDatatype({id: DATA_TYPE_PREFIX + '.Function', title: 'Function'})
export const QENUM = QDatatype({id: DATA_TYPE_PREFIX + '.Enum', title: 'Enum'})

export const DATA_TYPE_SELECTION_OPTIONS = [
	{value: QSTRING, text: QSTRING.title, id: QSTRING.id},
	{value: QNUMBER, text: QNUMBER.title, id: QNUMBER.id},
	{value: QBOOLEAN, text: QBOOLEAN.title, id: QBOOLEAN.id},
	{value: QDATE, text: QDATE.title, id: QDATE.id},
	{value: QOBJECT, text: QOBJECT.title, id: QOBJECT.id},
	{value: QARRAY, text: QARRAY.title, id: QARRAY.id},
	{value: QFUNCTION, text: QFUNCTION.title, id: QFUNCTION.id},
	{value: QENUM, text: QENUM.title, id: QENUM.id},
]
