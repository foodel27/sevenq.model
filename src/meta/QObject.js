import stampit from 'stampit'
import uuid from 'uuid/v4'
import {QType} from "./QType";
import {QSTRING} from "./QDataType";

export const TYPE_OBJECT = QType({
	id: 'com.sevenq.type.object',
	title: 'Object'
})

export const QObject = stampit({
	deepStatics: {
		MainLabel: TYPE_OBJECT.title,
		Type: TYPE_OBJECT,
		Attributes: {
			Id: {type: QSTRING},
			Title: {type: QSTRING},
		},
	},
	init({id, title},{ stamp }) {
		this.meta = stamp;
		this.id = id || uuid()
		this.title = title
	},
})