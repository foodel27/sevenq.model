// https://gist.github.com/jmar777/d151d7112059a1716aa1ecba699fe6fc
export const ENUM_ID_PREFIX = 'com.sevenq.enum'

export const QEnum = function (id, title, titles) {
	if( !id || !title || !titles || !titles.length)
		throw new Error(`Invalid ENUM arguments!`);

	let enumId = id;
	let enumTitle = title;
	let valueTitles = titles;
	let members = Object.create(null);
	let meta = {
		MainLabel : "Enum"
	}

	valueTitles.forEach(name => {
		if(!name)
			throw new Error(`Empty ENUM value name!`);
		members[name] = {
			id: enumId + '.' + name,
			title: name
		}
	});

	const enumProxy = new Proxy(members, {
		get: (target, name) => {
			if(name === 'id')
				return enumId
			if(name === 'title')
				return enumTitle
			if(name === 'meta')
				return meta
			if(name === 'selectOptions')
				return valueTitles.map(name => {
					return {
						value: members[name],
						text: name
					}
				})
			return members[name];
		},
		set: (target, name, value) => {
			throw new Error('Adding new members to Enums is not allowed.');
		}
	});

	return enumProxy;
}

