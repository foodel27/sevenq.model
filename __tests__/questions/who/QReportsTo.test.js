import {QReportsTo} from "../../../src/questions/who/QReportsTo";
import {QRelation} from "../../../src/word/QRelation";
import {QRole} from "../../../src/questions/who/QRole";


test('ReportsTo has META and Attributes', () => {
	let rel = QRelation({source: 'model1', target: 'trg1'})
	let repTo = QReportsTo({source: 'model2', target: 'trg2'})


	expect(rel.meta.MainLabel).toBe('Relation')
	expect(rel.meta.Parents.length).toBe(1)
	expect(rel.meta.Parents[0].title).toBe('Relation')
	expect(rel.title).toBe('Relation')
	expect(rel.source).toBe('model1')
	expect(rel.target).toBe('trg1')



	expect(repTo.meta.MainLabel).toBe('ReportsTo')
	expect(repTo.meta.Parents.length).toBe(2)
	expect(repTo.meta.Parents[0].title).toBe('Relation')
	expect(repTo.meta.Parents[1].title).toBe('ReportsTo')
	expect(repTo.meta.SourceTypes.length).toBe(1)
	expect(repTo.meta.SourceTypes[0]).toBe('Role')
	expect(repTo.title).toBe('ReportsTo')
	expect(repTo.source).toBe('model2')
	expect(repTo.target).toBe('trg2')

	let role = QRole()
	expect(role[QReportsTo.FieldName].length).toBe(0)

})
