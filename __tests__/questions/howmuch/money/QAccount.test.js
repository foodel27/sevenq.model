import {QAccount,TYPE_ACCOUNT,QParameter_Account_AddEntry_Entry,QEvent_Account_EntryAdded
} from "../../../../src/questions/howmuch/money/QAccount";
import {QAccountEntry, TYPE_ACCOUNT_ENTRY} from "../../../../src/questions/howmuch/money/QAccountEntry";
import {ENUM_MONEY_CURRENCIES, QMoney} from "../../../../src/questions/howmuch/money/QMoney";
import {QEventBus} from "../../../../src/flow/QEventBus";

jest.mock('../../../../src/flow/QEventBus')
QEventBus.emit.mockImplementation(()=>{console.log('mocked emitting')})


test('Parameter_AddEntry_Entry has attributes and META can clone', ()=>{
	let orig = QParameter_Account_AddEntry_Entry()
	expect(orig.title).toBe(TYPE_ACCOUNT.title + '#AddEntry.Entry')
	expect(orig.dataType.name).toBe('AccountEntry')

	// let cloned = Object.assign({},orig.meta(orig))
	// delete cloned.id
	// cloned = orig.meta(cloned)
	let cloned = orig.meta(orig)

	expect(cloned.title).toBe(TYPE_ACCOUNT.title + '#AddEntry.Entry')
	expect(cloned.dataType.name).toBe(TYPE_ACCOUNT_ENTRY.title)
	expect(cloned.required).toBe(true)
	// expect(cloned.id).toBe(orig.id)
})



test('EVENT_ENTRY_ADDED has attributes and META can clone', ()=>{
	let orig = QEvent_Account_EntryAdded()
	expect(orig.title).toBe(TYPE_ACCOUNT.title + '.EntryAdded')
	expect(orig.meta.name).toBe('Account.EntryAdded')
	let cloned = orig.meta(orig)

	expect(cloned.title).toBe(TYPE_ACCOUNT.title + '.EntryAdded')
	expect(cloned.meta.name).toBe('Account.EntryAdded')
})

test('Account AddEntry => Entries & Balance', () => {
	let acct1 = QAccount({
		initialBalance: 5
	})
	expect(acct1.balance).toBeDefined()
	expect(acct1.balance.amount).toBe(5)
	expect(acct1.balance.unit.title).toBe('USD')

	let acct2 = QAccount({
		currency: ENUM_MONEY_CURRENCIES.NIS
	})
	expect(acct2.balance).toBeDefined()
	expect(acct2.balance.amount).toBe(0)
	expect(acct2.balance.unit.title).toBe('NIS')

	let entry1 = QAccountEntry({
		amount: QMoney({amount: 10, unit: ENUM_MONEY_CURRENCIES.USD}),
		accountFrom: acct2,
		accountTo: acct1,
		description: 'acct2 => 10 USD => acct1'
	})

	acct1.addEntry(entry1)

	expect(acct1.balance.amount).toBe(15)
	expect(acct1.containsEntries.length).toBe(1)


	let entry2 = QAccountEntry({
		amount: QMoney({amount: 7, unit: ENUM_MONEY_CURRENCIES.USD}),
		accountFrom: acct1,
		accountTo: acct2,
		description: 'acct1 => 7 USD => acct2'
	})

	acct1.addEntry(entry2)
	expect(acct1.balance.amount).toBe(8)
	expect(acct1.containsEntries.length).toBe(2)

})




test('Account AddSubAccount => SubAccounts & Balance', () => {
	let acct1 = QAccount({
		initialBalance: 5
	})
	expect(acct1.balance).toBeDefined()
	expect(acct1.balance.amount).toBe(5)
	expect(acct1.balance.unit.title).toBe('USD')

	let acct2 = QAccount({
		initialBalance: 7
	})
	expect(acct2.balance).toBeDefined()
	expect(acct2.balance.amount).toBe(7)

	acct1.addSubAccount(acct2)
	expect(acct1.containsSubAccounts.length).toBe(1)
	expect(acct2.parentAccount.id).toBe(acct1.id)
	expect(acct1.balance.amount).toBe(12)


	let acct3 = QAccount({
		initialBalance: 20
	})
	expect(acct3.balance).toBeDefined()
	expect(acct3.balance.amount).toBe(20)
	// move from parent-to-parent
	acct3.addSubAccount(acct2)
	expect(acct3.balance.amount).toBe(27)
	// previous parent - should substract prev child
	expect(acct1.balance.amount).toBe(5)

})

