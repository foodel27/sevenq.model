import {QType} from "../../src/meta/QType";
import {QSTRING, QNUMBER, QDATE} from "../../src/meta/QDataType";

describe('QDataType suite', () =>{

	it('QDataType and QType has META', () => {
		expect(QSTRING.meta.MainLabel).toBe('DataType')
		expect(QNUMBER.meta.MainLabel).toBe('DataType')
		expect(QDATE.meta.MainLabel).toBe('DataType')
		let qtype = QType()
		expect(qtype.meta.MainLabel).toBe('Type')

	})
})
