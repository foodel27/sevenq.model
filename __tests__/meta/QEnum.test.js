import {QEnum} from "../../src/meta/QEnum"

test('QEnum throws on Invalid arguments', () => {
	expect(()=>{
		QEnum()
	}).toThrow();
	expect(()=>{
		QEnum('myId',)
	}).toThrow();
	expect(()=>{
		QEnum('myId','',['val1','val2'])
	}).toThrow();
	expect(()=>{
		QEnum('myId','myTitle',[])
	}).toThrow();
	expect(()=>{
		QEnum('myId','myTitle',['',''])
	}).toThrow();
})


test('QEnum throws on SET new Value', () => {
	expect( () => {
		QEnum().RED = '123'
	}).toThrow()
})


test('QEnum stores Values and Attributes', () => {

	let e1 = QEnum('myEnumId', 'myEnumTitle', ['val1','val2'])
	expect( e1 ).toEqual({
		val1: {
			id: "myEnumId.val1",
			title: "val1"
		},
		val2: {
			id: "myEnumId.val2",
			title: "val2"
		},
	})
	expect( e1.id ).toBe('myEnumId')
	expect( e1.title ).toBe('myEnumTitle')
	expect( e1.val1.id ).toBe('myEnumId.val1')
	expect( e1.val1.title ).toBe('val1')
	expect( e1.val2.id ).toBe('myEnumId.val2')
	expect( e1.val2.title ).toBe('val2')
})



test('QEnum has META', () => {
	let enum1 = QEnum('myId','myTitle',['adf','dasf'])
	expect(enum1.meta).toBeDefined()
	expect(enum1.meta.MainLabel).toBe('Enum')
})
