import {QCommand} from "../../src/flow/QCommand"
import {QEvent} from "../../src/flow/QEvent"
import {QEventBus} from "../../src/flow/QEventBus"

import {QAccount,QParameter_Account_AddEntry_Entry,QEvent_Account_EntryAdded
} from "../../src/questions/howmuch/money/QAccount";
import {QAccountEntry} from "../../src/questions/howmuch/money/QAccountEntry";
import {ENUM_MONEY_CURRENCIES, QMoney} from "../../src/questions/howmuch/money/QMoney";

jest.mock('../../../../src/flow/QEventBus')
QEventBus.emit.mockImplementation(()=>{console.log('mocked emitting')})

const

describe('Flow of Command & Event through EventBus', () => {
	// Command(Create account) -> Event(AccountCreated)
	// Command(Add account) -> Method(AddAcc) -> Event(AccountAdded)
	// Command(Remove account) -> Method(RemoveAcc) -> Event(AccountRemoved)
	// Command(Add account w Parent)-> Method(newParentAcc.AddAcc)-> Method(prevParentAcc.RemoveAcc) ---->
	// 							-----> Event(AccountRemoved) + Event(AccountAdded)


	// play events - same data
	// show cause-effect chain
})