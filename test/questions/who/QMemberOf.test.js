import {QRelation} from "../../../src/word/QRelation";
import {QRole} from "../../../src/questions/who/QRole";
import {QMemberOf} from "../../../src/questions/who/QMemberOf";


test('MemberOf has META and Attributes', () => {
	let rel = QRelation({source: 'model1', target: 'trg1'})
	let repTo = QMemberOf({source: 'model2', target: 'trg2'})


	expect(rel.meta.MainLabel).toBe('Relation')
	expect(rel.meta.Parents.length).toBe(1)
	expect(rel.meta.Parents[0].title).toBe('Relation')
	expect(rel.title).toBe('Relation')
	expect(rel.source).toBe('model1')
	expect(rel.target).toBe('trg1')



	expect(repTo.meta.MainLabel).toBe('MemberOf')
	expect(repTo.meta.Parents.length).toBe(2)
	expect(repTo.meta.Parents[0].title).toBe('Relation')
	expect(repTo.meta.Parents[1].title).toBe('MemberOf')
	expect(repTo.meta.SourceTypes.length).toBe(1)
	expect(repTo.meta.SourceTypes[0]).toBe('Role')
	expect(repTo.title).toBe('MemberOf')
	expect(repTo.source).toBe('model2')
	expect(repTo.target).toBe('trg2')

	let role = QRole()
	expect(role[QMemberOf.FieldName].length).toBe(0)

})
